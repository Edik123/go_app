# Bookings and Reservations project

Used packages

- [chi router](https://github.com/go-chi/chi)
- [scs](https://github.com/alexedwards/scs)
- [nosurf](https://github.com/justinas/nosurf)
- [govalidator](https://github.com/asaskevich/govalidator)
- [pgx](https://github.com/jackc/pgx)
- [soda](https://gobuffalo.io/en/docs/db/toolbox)
- [go-simple-mail](https://github.com/xhit/go-simple-mail)
- [Simple-DataTables](https://github.com/fiduswriter/Simple-DataTables)
- [RoyalUI-Free-Bootstrap-Admin-Template](https://github.com/BootstrapDash/RoyalUI-Free-Bootstrap-Admin-Template)