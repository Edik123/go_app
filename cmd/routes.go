package main

import(
	"net/http"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"bitbucket.org/Edik123/go_app/src/master/internal/config"
	"bitbucket.org/Edik123/go_app/src/master/internal/handlers"
)

func routes(app *config.AppConfig) http.Handler{
	r := chi.NewRouter()

	r.Use(middleware.Recoverer)
	r.Use(NoSurf)
	r.Use(SessionLoad)

	r.Get("/", handlers.Repo.Home)
	r.Get("/about", handlers.Repo.About)
	r.Get("/contact", handlers.Repo.Contact)

	r.Get("/login", handlers.Repo.Login)
	r.Post("/login", handlers.Repo.PostLogin)
	r.Get("/logout", handlers.Repo.Logout)

	r.Get("/search", handlers.Repo.Search)
	r.Post("/search", handlers.Repo.PostSearch)
	r.Post("/search-json", handlers.Repo.SearchJson)
	r.Get("/choose-room/{id}", handlers.Repo.ChooseRoom)
	r.Get("/book-room", handlers.Repo.BookRoom)

	r.Get("/generals", handlers.Repo.Generals)
	r.Get("/majors", handlers.Repo.Majors)

	r.Get("/reservation", handlers.Repo.Reservation)
	r.Post("/reservation", handlers.Repo.PostReservation)
	r.Get("/reservation-summary", handlers.Repo.ReservationSummary)

	fileServer := http.FileServer(http.Dir("./static/"))
	r.Handle("/static/*", http.StripPrefix("/static", fileServer))

	r.Route("/admin", func(r chi.Router) {
		//r.Use(Auth)
		r.Get("/dashboard", handlers.Repo.AdminDashboard)

		r.Get("/reservations-new", handlers.Repo.AdminNewReservations)
		r.Get("/reservations-all", handlers.Repo.AdminAllReservations)
		r.Get("/reservations-calendar", handlers.Repo.AdminReservationsCalendar)

		r.Get("/reservations/{src}/{id}/show", handlers.Repo.AdminShowReservation)
		r.Post("/reservations/{src}/{id}", handlers.Repo.AdminPostShowReservation)
		r.Get("/process-reservation/{src}/{id}", handlers.Repo.AdminProcessReservation)
		r.Get("/delete-reservation/{src}/{id}", handlers.Repo.AdminDeleteReservation)
	})

	return r
}