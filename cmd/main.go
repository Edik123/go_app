package main

import (
	"os"
	"fmt"
	"log"
	"time"
	"flag"
	"net/http"
	"encoding/gob"
	"github.com/alexedwards/scs/v2"
	"bitbucket.org/Edik123/go_app/src/master/internal/dto"
	"bitbucket.org/Edik123/go_app/src/master/internal/config"
	"bitbucket.org/Edik123/go_app/src/master/internal/render"
	"bitbucket.org/Edik123/go_app/src/master/internal/handlers"
	"bitbucket.org/Edik123/go_app/src/master/internal/helpers"
	"bitbucket.org/Edik123/go_app/src/master/internal/services"
	"bitbucket.org/Edik123/go_app/src/master/internal/database"
)

const postNumber = ":8000"

var app      config.AppConfig
var session  *scs.SessionManager
var infoLog  *log.Logger
var errorLog *log.Logger

func main() {
	// Only types that will be transferred as implementations of interface values need to be registered
	gob.Register(dto.User{})
	gob.Register(dto.Room{})
	gob.Register(dto.Restriction{})
	gob.Register(dto.Reservation{})

    // flags
	inProduction := flag.Bool("production", false, "Application is in production")
	useCache := flag.Bool("cache", false, "Use template cache")

	dbHost := flag.String("dbhost", "localhost", "Database host")
	dbName := flag.String("dbname", "bookings", "Database name")
	dbUser := flag.String("dbuser", "postgres", "Database user")
	dbPass := flag.String("dbpass", "", "Database password")
	dbPort := flag.String("dbport", "5432", "Database port")
	dbSSL := flag.String("dbssl", "disable", "Database ssl settings (disable, prefer, require)")

	flag.Parse()

	if *dbName == "" || *dbUser == "" {
		fmt.Println("Missing required flags")
		os.Exit(1)
	}

	// change this to true when in production
	app.InProduction = *inProduction
	app.UseCache = *useCache

	infoLog = log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	app.InfoLog = infoLog

	errorLog = log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
	app.ErrorLog = errorLog

    fmt.Println("Staring mail listener...")
	mailChan := make(chan dto.MailData)
	app.MailChan = mailChan
	services.NewServices(&app)
	services.ListenForMail()
	defer close(app.MailChan)

	session = scs.New()
	session.Lifetime = 24 * time.Hour
	session.Cookie.Persist = true
    session.Cookie.SameSite = http.SameSiteLaxMode
    session.Cookie.Secure = app.InProduction

    app.Session = session

    /*from := "ed_va@mail.ru"
    auth := smtp.PlainAuth("", from, "", "localhost")
    err = smtp.SendMail("localhost:1025", auth, from, []string{"you@there.com"}, []byte("Hello world!")) 
    if err != nil {
    	log.Println(err)
    }*/

    // connect to database
	log.Println("Connecting to database...")
	//db, err := database.ConnectSQL("host=localhost port=5432 dbname=bookings user=postgres password=")
	connectionString := fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=%s", *dbHost, *dbPort, *dbName, *dbUser, *dbPass, *dbSSL)
	db, err := database.ConnectSQL(connectionString)
	if err != nil {
		log.Fatal("Cannot connect to database!")
	}
	log.Println("Connected to database!")
	defer db.SQL.Close()

	cache, err := render.CreateTemplateCache()
	if err != nil {
		log.Fatal("Cannot create template cache", err)
	}

	app.TemplateCache = cache

	repo := handlers.NewRepo(&app, db)
	handlers.NewHandlers(repo)
	helpers.NewHelpers(&app)

	render.NewTemplate(&app)

    fmt.Println(fmt.Sprintf("Starting application on port %s", postNumber))
	_ = http.ListenAndServe(postNumber, routes(&app))
}