package main

import (
	"fmt"
	"net/http"
	"github.com/justinas/nosurf"
	"bitbucket.org/Edik123/go_app/src/master/internal/helpers"
)

func WriteToConsole(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Page is loaded")
		h.ServeHTTP(w, r)
	})
}

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !helpers.IsAuthenticated(r) {
			session.Put(r.Context(), "error", "Log in first!")
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return
		}
		next.ServeHTTP(w, r)
	})
}

// implements CSRF protection to all Post request
func NoSurf(h http.Handler) http.Handler {
	csrfHandler := nosurf.New(h)

	csrfHandler.SetBaseCookie(http.Cookie{
		HttpOnly: true,
		Path:     "/",
		Secure:   app.InProduction,
		SameSite: http.SameSiteLaxMode,
	})

	return csrfHandler
}

// automatic loading and saving of session on every request via middleware
func SessionLoad(h http.Handler) http.Handler {
	return session.LoadAndSave(h)
}