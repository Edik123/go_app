package services

import (
	"fmt"
	"log"
	"time"
	"strings"
	"io/ioutil"
	"bitbucket.org/Edik123/go_app/src/master/internal/dto"
	"bitbucket.org/Edik123/go_app/src/master/internal/config"
	mail "github.com/xhit/go-simple-mail/v2"
)

var app *config.AppConfig

// sets config for services
func NewServices(a *config.AppConfig) {
	app = a
}

func ListenForMail() {
	go func() {
		for {
			msg := <-app.MailChan
			sendMsg(msg)
		}
	}()
}

func sendMsg(msg dto.MailData) {
	server := mail.NewSMTPClient()

	// SMTP Server
	server.Host = "localhost"
	server.Port = 1025
	server.KeepAlive = false
	server.ConnectTimeout = 10 * time.Second
	server.SendTimeout = 10 * time.Second

    // SMTP client
	client, err := server.Connect()
	if err != nil {
		app.ErrorLog.Println(err)
	}

	email := mail.NewMSG()
	email.SetFrom(msg.From).AddTo(msg.To).SetSubject(msg.Subject)

	if msg.Template == "" {
		email.SetBody(mail.TextHTML, msg.Content)
	} else {
		data, err := ioutil.ReadFile(fmt.Sprintf("./templates/%s", msg.Template))
		if err != nil {
			app.ErrorLog.Println(err)
		}

		mailTemplate := string(data)
		msgToSend := strings.Replace(mailTemplate, "[%body%]", msg.Content, 1)
		email.SetBody(mail.TextHTML, msgToSend)
	}

	err = email.Send(client)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Email sent!")
	}
}