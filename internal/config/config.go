package config

import (
	"log"
	"html/template"
	"github.com/alexedwards/scs/v2"
	"bitbucket.org/Edik123/go_app/src/master/internal/dto"
)

type AppConfig struct{
	UseCache      bool
	InProduction  bool
	InfoLog       *log.Logger
	ErrorLog      *log.Logger
	Session       *scs.SessionManager
	MailChan      chan dto.MailData
	TemplateCache map[string]*template.Template
}