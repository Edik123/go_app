package database

import (
	"time"
	"bitbucket.org/Edik123/go_app/src/master/internal/dto"
)

type Database interface {
	AllUsers() bool

	InsertReservation(r dto.Reservation) (int, error)
	InsertRoomRestriction(r dto.RoomRestriction) error
	SearchAvailabilityByDatesByRoomID(start, end time.Time, roomID int) (bool, error)
	SearchAvailabilityForAllRooms(start, end time.Time) ([]dto.Room, error)
	GetRoomByID(id int) (dto.Room, error)

	GetUserByID(id int) (dto.User, error)
	UpdateUser(user dto.User) error
	Authenticate(email, testPassword string) (int, string, error)

	AllReservations() ([]dto.Reservation, error)
	NewReservations() ([]dto.Reservation, error)
	GetReservationByID(id int) (dto.Reservation, error)
	UpdateReservation(u dto.Reservation) error
	DeleteReservation(id int) error
	UpdateProcessedForReservation(id, processed int) error
}