package handlers

import (
	"net/http"
	"bitbucket.org/Edik123/go_app/src/master/internal/dto"
	"bitbucket.org/Edik123/go_app/src/master/internal/render"
	"bitbucket.org/Edik123/go_app/src/master/internal/config"
	"bitbucket.org/Edik123/go_app/src/master/internal/database"
	"bitbucket.org/Edik123/go_app/src/master/internal/database/dbrepo"
)

var Repo *Repository

type Repository struct{
	App *config.AppConfig
	DB database.Database
}

func NewRepo(a *config.AppConfig, db *database.DB) *Repository{
	return &Repository{
		App: a,
		DB:  dbrepo.NewPostgresRepo(db.SQL, a),
	}
}

// repository for the handlers
func NewHandlers(r *Repository){
	Repo = r
}

func (m *Repository) Home(w http.ResponseWriter, r *http.Request){
	render.RenderTemplate(w, r, "home.page.tmpl", &dto.TemplateData{})
}

func (m *Repository) About(w http.ResponseWriter, r *http.Request){
	render.RenderTemplate(w, r, "about.page.tmpl", &dto.TemplateData{})
}

func (t *Repository) Contact(w http.ResponseWriter, r *http.Request){
	stringMap := make(map[string]string)
	stringMap["phone"] = "+19025551212"

	render.RenderTemplate(w, r, "contact.page.tmpl", &dto.TemplateData{
		StringMap: stringMap,
	})
}

func (m *Repository) Generals(w http.ResponseWriter, r *http.Request) {
	render.RenderTemplate(w, r, "generals.page.tmpl", &dto.TemplateData{})
}

func (m *Repository) Majors(w http.ResponseWriter, r *http.Request) {
	render.RenderTemplate(w, r, "majors.page.tmpl", &dto.TemplateData{})
}