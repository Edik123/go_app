package render

import (
	"fmt"
	"log"
	"time"
	"bytes"
	"net/http"
	"html/template"
	"path/filepath"
	"github.com/justinas/nosurf"
	"bitbucket.org/Edik123/go_app/src/master/internal/dto"
	"bitbucket.org/Edik123/go_app/src/master/internal/config"
	"bitbucket.org/Edik123/go_app/src/master/internal/helpers"
)

var functions = template.FuncMap{
	"humanDate": HumanDate,
}

var app *config.AppConfig

// config for the template package
func NewTemplate(a *config.AppConfig){
	app = a
}

// eturns time in YYYY-MM-DD format
func HumanDate(t time.Time) string {
	return t.Format("2006-01-02")
}

// Adds basic data for all templates
func AddDefaultData(td *dto.TemplateData, r *http.Request) *dto.TemplateData {
	td.CSRFToken = nosurf.Token(r)
	td.Flash = app.Session.PopString(r.Context(), "flash")
	td.Error = app.Session.PopString(r.Context(), "error")
	td.Warning = app.Session.PopString(r.Context(), "warning")

	if helpers.IsAuthenticated(r) {
		td.IsAuthenticated = 1
	}
	return td
}

func RenderTemplate(w http.ResponseWriter, r *http.Request, tmpl string, data *dto.TemplateData){

	var cache map[string]*template.Template
	if app.UseCache{
		cache = app.TemplateCache
	} else {
		cache, _ = CreateTemplateCache()
	}

	t, ok := cache[tmpl]
	if !ok{
		log.Fatal("Could not get template from cache")
	}

	buf := new(bytes.Buffer)

	data = AddDefaultData(data, r)

	_ = t.Execute(buf, data)

	_, err := buf.WriteTo(w)
	if err != nil {
		fmt.Println("Error writing template to browser", err)
		return
	}
}

func CreateTemplateCache() (map[string]*template.Template, error){

	myCache := map[string]*template.Template{}

	pages, err := filepath.Glob("./templates/*.page.tmpl")
	if err != nil {
		return myCache, err
	}

	for _, page := range pages{
		name := filepath.Base(page)

		ts, err := template.New(name).Funcs(functions).ParseFiles(page)
		if err != nil {
		    return myCache, err
	    }

	    matches, err := filepath.Glob("./templates/*.layout.tmpl")
		if err != nil {
		    return myCache, err
	    }

	    if len(matches) > 0 {
	    	ts, err = ts.ParseGlob("./templates/*.layout.tmpl")
			if err != nil {
			    return myCache, err
		    }
	    }

	    myCache[name] = ts
	}

	return myCache, nil;
}